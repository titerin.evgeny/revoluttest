Revolut Test Application

This is my implementation of Revolut task, which can be found here:
https://docs.google.com/document/d/13Ecs3hhgZJJLsugNUwZPUn_9gsqzwH80Bb-1CRbauTQ/edit

This task is actually not fully complete.
My plans for it are:

* <s>Add presentation model and mapper from domain model: to remove BigDecimal formatting from TextWatcher and ViewHolder,
    and here can also be added additional data such as currency description, image, etc</s>
* Add currency description and flag image urls to presentation model
* Improve ViewHolder item (add image and currency description)
* Clean build.gradle files
* Improve MVP with some MVP-lib like Moxy (for now this is just primitive implementation)
* Add instrumental testing
* Add useful gradle tasks (for launching all tests, for example)
* Add lint checks

I used kotlin reflection to map data model to domain model just because this is less messy, this can be removed if there will be performance issues



package net.etiterin.domain.repository

import io.reactivex.Single
import net.etiterin.domain.model.CurrencyRate

interface CurrencyRatesRepository {

    fun getCurrencyRates(baseCurrencyCode: String, isCached: Boolean = false): Single<MutableList<CurrencyRate>>
}
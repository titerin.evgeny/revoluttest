package net.etiterin.domain.model

import java.math.BigDecimal

data class CurrencyRate(
    val currencyCode: String,
    var rateValue: BigDecimal
)
package net.etiterin.domain.usecase

import io.reactivex.Single
import net.etiterin.domain.model.CurrencyRate
import net.etiterin.domain.repository.CurrencyRatesRepository
import java.math.BigDecimal

private typealias RatesList = MutableList<CurrencyRate>

class CurrencyRatesUseCase(private val ratesRepository: CurrencyRatesRepository) {

    fun getCurrencyRates(baseRate: CurrencyRate, isCached: Boolean = false): Single<RatesList> {
        val baseCurrencyCode = baseRate.currencyCode
        val baseRateValue = baseRate.rateValue
        return ratesRepository.getCurrencyRates(baseCurrencyCode, isCached)
            .map { rates ->
                multiplyRatesByBaseCurrencyValue(baseRateValue, rates)
                addBaseRateAsFirstElement(rates, baseRate)
                rates
            }
    }

    private fun addBaseRateAsFirstElement(rates: RatesList, baseRate: CurrencyRate) = rates.apply {
        add(0, baseRate)
    }

    private fun multiplyRatesByBaseCurrencyValue(
        baseCurrencyValue: BigDecimal,
        ratesList: RatesList
    ): RatesList {
        return ratesList.apply {
            forEach { rate ->
                rate.rateValue = baseCurrencyValue.multiply(rate.rateValue).stripTrailingZeros()
            }
        }
    }
}
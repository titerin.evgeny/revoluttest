package net.etiterin.domain.model

import java.math.BigDecimal

class CurrencyRatesData {

    companion object {

        val exampleCurrencyRatesList = mutableListOf(
            CurrencyRate(currencyCode = "AUD", rateValue = BigDecimal("0.020352")),
            CurrencyRate(currencyCode = "BGN", rateValue = BigDecimal("0.024625")),
            CurrencyRate(currencyCode = "BRL", rateValue = BigDecimal("0.060333")),
            CurrencyRate(currencyCode = "CAD", rateValue = BigDecimal("0.019311")),
            CurrencyRate(currencyCode = "CHF", rateValue = BigDecimal("0.014197")),
            CurrencyRate(currencyCode = "CNY", rateValue = BigDecimal("0.10004")),
            CurrencyRate(currencyCode = "CZK", rateValue = BigDecimal("0.32377")),
            CurrencyRate(currencyCode = "DKK", rateValue = BigDecimal("0.093886")),
            CurrencyRate(currencyCode = "EUR", rateValue = BigDecimal("0.012591")),
            CurrencyRate(currencyCode = "GBP", rateValue = BigDecimal("0.01131")),
            CurrencyRate(currencyCode = "HKD", rateValue = BigDecimal("0.11498")),
            CurrencyRate(currencyCode = "HRK", rateValue = BigDecimal("0.093601")),
            CurrencyRate(currencyCode = "HUF", rateValue = BigDecimal("4.1107")),
            CurrencyRate(currencyCode = "IDR", rateValue = BigDecimal("218.12")),
            CurrencyRate(currencyCode = "ILS", rateValue = BigDecimal("0.052512")),
            CurrencyRate(currencyCode = "INR", rateValue = BigDecimal("1.0541")),
            CurrencyRate(currencyCode = "ISK", rateValue = BigDecimal("1.609")),
            CurrencyRate(currencyCode = "JPY", rateValue = BigDecimal("1.6312")),
            CurrencyRate(currencyCode = "KRW", rateValue = BigDecimal("16.429")),
            CurrencyRate(currencyCode = "MXN", rateValue = BigDecimal("0.2816")),
            CurrencyRate(currencyCode = "MYR", rateValue = BigDecimal("0.060587")),
            CurrencyRate(currencyCode = "NOK", rateValue = BigDecimal("0.12309")),
            CurrencyRate(currencyCode = "NZD", rateValue = BigDecimal("0.022202")),
            CurrencyRate(currencyCode = "PHP", rateValue = BigDecimal("0.78809")),
            CurrencyRate(currencyCode = "PLN", rateValue = BigDecimal("0.054371")),
            CurrencyRate(currencyCode = "RON", rateValue = BigDecimal("0.058403")),
            CurrencyRate(currencyCode = "SEK", rateValue = BigDecimal("0.13335")),
            CurrencyRate(currencyCode = "SGD", rateValue = BigDecimal("0.020146")),
            CurrencyRate(currencyCode = "THB", rateValue = BigDecimal("0.48009")),
            CurrencyRate(currencyCode = "TRY", rateValue = BigDecimal("0.096045")),
            CurrencyRate(currencyCode = "USD", rateValue = BigDecimal("0.014648")),
            CurrencyRate(currencyCode = "ZAR", rateValue = BigDecimal("0.2244"))
        )

        val exampleBaseRate = CurrencyRate("RUB", BigDecimal("100"))

        val processedCurrencyRatesList = mutableListOf(
            CurrencyRate(currencyCode = "RUB", rateValue = BigDecimal("100")),
            CurrencyRate(currencyCode = "AUD", rateValue = BigDecimal("2.0352")),
            CurrencyRate(currencyCode = "BGN", rateValue = BigDecimal("2.4625")),
            CurrencyRate(currencyCode = "BRL", rateValue = BigDecimal("6.0333")),
            CurrencyRate(currencyCode = "CAD", rateValue = BigDecimal("1.9311")),
            CurrencyRate(currencyCode = "CHF", rateValue = BigDecimal("1.4197")),
            CurrencyRate(currencyCode = "CNY", rateValue = BigDecimal("10.004")),
            CurrencyRate(currencyCode = "CZK", rateValue = BigDecimal("32.377")),
            CurrencyRate(currencyCode = "DKK", rateValue = BigDecimal("9.3886")),
            CurrencyRate(currencyCode = "EUR", rateValue = BigDecimal("1.2591")),
            CurrencyRate(currencyCode = "GBP", rateValue = BigDecimal("1.131")),
            CurrencyRate(currencyCode = "HKD", rateValue = BigDecimal("11.498")),
            CurrencyRate(currencyCode = "HRK", rateValue = BigDecimal("09.3601")),
            CurrencyRate(currencyCode = "HUF", rateValue = BigDecimal("411.07")),
            CurrencyRate(currencyCode = "IDR", rateValue = BigDecimal("21812.")),
            CurrencyRate(currencyCode = "ILS", rateValue = BigDecimal("5.2512")),
            CurrencyRate(currencyCode = "INR", rateValue = BigDecimal("105.41")),
            CurrencyRate(currencyCode = "ISK", rateValue = BigDecimal("160.9")),
            CurrencyRate(currencyCode = "JPY", rateValue = BigDecimal("163.12")),
            CurrencyRate(currencyCode = "KRW", rateValue = BigDecimal("1642.9")),
            CurrencyRate(currencyCode = "MXN", rateValue = BigDecimal("28.16")),
            CurrencyRate(currencyCode = "MYR", rateValue = BigDecimal("6.0587")),
            CurrencyRate(currencyCode = "NOK", rateValue = BigDecimal("12.309")),
            CurrencyRate(currencyCode = "NZD", rateValue = BigDecimal("2.2202")),
            CurrencyRate(currencyCode = "PHP", rateValue = BigDecimal("78.809")),
            CurrencyRate(currencyCode = "PLN", rateValue = BigDecimal("5.4371")),
            CurrencyRate(currencyCode = "RON", rateValue = BigDecimal("5.8403")),
            CurrencyRate(currencyCode = "SEK", rateValue = BigDecimal("13.335")),
            CurrencyRate(currencyCode = "SGD", rateValue = BigDecimal("2.0146")),
            CurrencyRate(currencyCode = "THB", rateValue = BigDecimal("48.009")),
            CurrencyRate(currencyCode = "TRY", rateValue = BigDecimal("9.6045")),
            CurrencyRate(currencyCode = "USD", rateValue = BigDecimal("1.4648")),
            CurrencyRate(currencyCode = "ZAR", rateValue = BigDecimal("22.44"))
        )
    }
}
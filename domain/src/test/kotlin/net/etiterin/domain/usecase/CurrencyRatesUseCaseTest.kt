package net.etiterin.domain.usecase

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import net.etiterin.domain.model.CurrencyRatesData.Companion.exampleBaseRate
import net.etiterin.domain.model.CurrencyRatesData.Companion.exampleCurrencyRatesList
import net.etiterin.domain.model.CurrencyRatesData.Companion.processedCurrencyRatesList
import net.etiterin.domain.repository.CurrencyRatesRepository
import org.junit.Test

class CurrencyRatesUseCaseTest {

    private val repository: CurrencyRatesRepository = mock()

    private val useCase = CurrencyRatesUseCase(repository)

    @Test
    fun `should get processed currency rates list`() {
        val expectedRatesList = processedCurrencyRatesList
        val repoRatesListSingle = Single.just(exampleCurrencyRatesList)
        whenever(repository.getCurrencyRates("RUB")).thenReturn(repoRatesListSingle)

        val testObserver = useCase.getCurrencyRates(baseRate = exampleBaseRate).test()

        testObserver.assertResult(expectedRatesList)
        testObserver.assertComplete()
    }
}
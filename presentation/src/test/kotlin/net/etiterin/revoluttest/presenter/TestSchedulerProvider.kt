package net.etiterin.revoluttest.presenter

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import net.etiterin.revoluttest.rx.SchedulerProvider

class TestSchedulerProvider : SchedulerProvider {

    override fun androidMainScheduler(): Scheduler {
        return Schedulers.trampoline()
    }

    override fun ioScheduler(): Scheduler {
        return Schedulers.trampoline()
    }
}
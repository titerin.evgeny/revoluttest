package net.etiterin.revoluttest.presenter

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import net.etiterin.domain.model.CurrencyRate
import net.etiterin.domain.model.CurrencyRatesData.Companion.exampleBaseRate
import net.etiterin.domain.model.CurrencyRatesData.Companion.exampleCurrencyRatesList
import net.etiterin.domain.repository.CurrencyRatesRepository
import net.etiterin.domain.usecase.CurrencyRatesUseCase
import net.etiterin.revoluttest.mapper.CurrencyRateItemMapper
import net.etiterin.revoluttest.model.exampleCurrencyRatesItemList
import net.etiterin.revoluttest.model.examplePresentationRate
import net.etiterin.revoluttest.view.MainView
import org.junit.Before
import org.junit.Test
import java.io.IOException

private typealias DomainRatesList = MutableList<CurrencyRate>

class MainPresenterTest {

    private val repository: CurrencyRatesRepository = mock()
    private val view: MainView = mock()
    private val mapper: CurrencyRateItemMapper = mock()
    private val useCase = CurrencyRatesUseCase(repository)
    private val testSchedulerProvider = TestSchedulerProvider()

    private val presenter = MainPresenter(useCase, testSchedulerProvider, mapper)

    @Before
    fun setup() {
        presenter.start(view)
        whenever(mapper.mapFromDomainList(exampleCurrencyRatesList)).thenReturn(exampleCurrencyRatesItemList)
        whenever(mapper.mapToDomainRate(examplePresentationRate)).thenReturn(exampleBaseRate)
    }

    @Test
    fun `should call showError if error occurred`() {
        val expectedErrorText = "Example error text"
        val errorSingle = Single.error<DomainRatesList>(IOException(expectedErrorText))
        val dataSingle = Single.just(exampleCurrencyRatesList)
        whenever(repository.getCurrencyRates(baseCurrencyCode = "RUB")).thenReturn(dataSingle)
        whenever(repository.getCurrencyRates(baseCurrencyCode = "RUB", isCached = true)).thenReturn(errorSingle)

        presenter.loadRates(examplePresentationRate)

        verify(view).showError(expectedErrorText)
    }

    @Test
    fun `should show loaded data`() {
        val expectedRatesList = exampleCurrencyRatesItemList
        val dataSingle = Single.just(exampleCurrencyRatesList)
        val emptySingle = Single.just(mutableListOf<CurrencyRate>())
        whenever(repository.getCurrencyRates(baseCurrencyCode = "RUB")).thenReturn(emptySingle)
        whenever(repository.getCurrencyRates(baseCurrencyCode = "RUB", isCached = true)).thenReturn(dataSingle)

        presenter.loadRates(examplePresentationRate)

        verify(view).showCurrencyRates(expectedRatesList)
    }
}
package net.etiterin.revoluttest.mapper

import junit.framework.Assert.assertEquals
import net.etiterin.domain.model.CurrencyRatesData.Companion.exampleBaseRate
import net.etiterin.domain.model.CurrencyRatesData.Companion.processedCurrencyRatesList
import net.etiterin.revoluttest.model.exampleCurrencyRatesItemList
import net.etiterin.revoluttest.model.examplePresentationRate
import org.junit.Test

class CurrencyRateItemMapperImplTest {

    private val mapper = CurrencyRateItemMapperImpl()

    @Test
    fun `should map domain list to presentation list`() {
        val domainProcessedList = processedCurrencyRatesList
        val expectedList = exampleCurrencyRatesItemList

        val actualList = mapper.mapFromDomainList(domainProcessedList)

        assertEquals(expectedList, actualList)
    }

    @Test
    fun `should map presentation rate to domain rate`() {
        val presentationRate = examplePresentationRate
        val expectedDomainRate = exampleBaseRate

        val actualDomainRate = mapper.mapToDomainRate(presentationRate)

        assertEquals(expectedDomainRate, actualDomainRate)
    }
}
package net.etiterin.revoluttest.model

val exampleCurrencyRatesItemList = mutableListOf(
    CurrencyRateItem(currencyCode = "RUB", rateValue = "100.00"),
    CurrencyRateItem(currencyCode = "AUD", rateValue = "2.04"),
    CurrencyRateItem(currencyCode = "BGN", rateValue = "2.46"),
    CurrencyRateItem(currencyCode = "BRL", rateValue = "6.03"),
    CurrencyRateItem(currencyCode = "CAD", rateValue = "1.93"),
    CurrencyRateItem(currencyCode = "CHF", rateValue = "1.42"),
    CurrencyRateItem(currencyCode = "CNY", rateValue = "10.00"),
    CurrencyRateItem(currencyCode = "CZK", rateValue = "32.38"),
    CurrencyRateItem(currencyCode = "DKK", rateValue = "9.39"),
    CurrencyRateItem(currencyCode = "EUR", rateValue = "1.26"),
    CurrencyRateItem(currencyCode = "GBP", rateValue = "1.13"),
    CurrencyRateItem(currencyCode = "HKD", rateValue = "11.50"),
    CurrencyRateItem(currencyCode = "HRK", rateValue = "9.36"),
    CurrencyRateItem(currencyCode = "HUF", rateValue = "411.07"),
    CurrencyRateItem(currencyCode = "IDR", rateValue = "21812.00"),
    CurrencyRateItem(currencyCode = "ILS", rateValue = "5.25"),
    CurrencyRateItem(currencyCode = "INR", rateValue = "105.41"),
    CurrencyRateItem(currencyCode = "ISK", rateValue = "160.90"),
    CurrencyRateItem(currencyCode = "JPY", rateValue = "163.12"),
    CurrencyRateItem(currencyCode = "KRW", rateValue = "1642.90"),
    CurrencyRateItem(currencyCode = "MXN", rateValue = "28.16"),
    CurrencyRateItem(currencyCode = "MYR", rateValue = "6.06"),
    CurrencyRateItem(currencyCode = "NOK", rateValue = "12.31"),
    CurrencyRateItem(currencyCode = "NZD", rateValue = "2.22"),
    CurrencyRateItem(currencyCode = "PHP", rateValue = "78.81"),
    CurrencyRateItem(currencyCode = "PLN", rateValue = "5.44"),
    CurrencyRateItem(currencyCode = "RON", rateValue = "5.84"),
    CurrencyRateItem(currencyCode = "SEK", rateValue = "13.34"),
    CurrencyRateItem(currencyCode = "SGD", rateValue = "2.01"),
    CurrencyRateItem(currencyCode = "THB", rateValue = "48.01"),
    CurrencyRateItem(currencyCode = "TRY", rateValue = "9.60"),
    CurrencyRateItem(currencyCode = "USD", rateValue = "1.46"),
    CurrencyRateItem(currencyCode = "ZAR", rateValue = "22.44")
)

val examplePresentationRate = CurrencyRateItem(currencyCode = "RUB", rateValue = "100")
package net.etiterin.revoluttest.model

data class CurrencyRateItem(
    val currencyCode: String,
    var rateValue: String
)
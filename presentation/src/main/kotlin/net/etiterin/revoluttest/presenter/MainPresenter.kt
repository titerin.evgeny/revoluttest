package net.etiterin.revoluttest.presenter

import io.reactivex.Single
import io.reactivex.SingleTransformer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import net.etiterin.domain.model.CurrencyRate
import net.etiterin.domain.usecase.CurrencyRatesUseCase
import net.etiterin.revoluttest.mapper.CurrencyRateItemMapper
import net.etiterin.revoluttest.mapper.PresentationRateList
import net.etiterin.revoluttest.model.CurrencyRateItem
import net.etiterin.revoluttest.rx.SchedulerProvider
import net.etiterin.revoluttest.view.MainView
import java.util.concurrent.TimeUnit

private val DEFAULT_RATE = CurrencyRateItem(currencyCode = "EUR", rateValue = "100")
private const val ONE_SECOND = 1000L

class MainPresenter(
    private val ratesUseCase: CurrencyRatesUseCase,
    private val schedulerProvider: SchedulerProvider,
    private val mapper: CurrencyRateItemMapper
) {

    private val disposables = CompositeDisposable()
    private lateinit var view: MainView

    fun start(view: MainView) {
        this.view = view
    }

    fun loadRates(baseRate: CurrencyRateItem = DEFAULT_RATE) {
        unSubscribeAll()
        getCachedRatesThenRepeatGettingFromRemote(baseRate)
    }

    fun unSubscribeAll() {
        disposables.clear()
    }

    private fun getCachedRatesThenRepeatGettingFromRemote(baseRate: CurrencyRateItem) {
        disposables.addAll(
            getCachedRates(baseRate),
            getLatestRates(baseRate)
        )
    }

    private fun getCachedRates(baseRate: CurrencyRateItem): Disposable {
        return createListSingle(baseRate, cached = true)
            .subscribe(::showRates, ::showError)
    }

    private fun createListSingle(baseRate: CurrencyRateItem, cached: Boolean = false): Single<PresentationRateList> {
        return toDomainRate(baseRate).flatMap { domainRate ->
            ratesUseCase.getCurrencyRates(baseRate = domainRate, isCached = cached)
        }
            .map { list -> mapper.mapFromDomainList(list) }
            .compose(applySchedulers())
    }

    private fun <T> applySchedulers() = SingleTransformer<T, T> { single ->
        single
            .subscribeOn(schedulerProvider.ioScheduler())
            .observeOn(schedulerProvider.androidMainScheduler())
    }

    private fun toDomainRate(baseRate: CurrencyRateItem): Single<CurrencyRate> {
        return Single.fromCallable { baseRate }
            .map { presentationRate ->
                mapper.mapToDomainRate(presentationRate)
            }
    }

    private fun getLatestRates(baseRate: CurrencyRateItem): Disposable {
        return createListSingle(baseRate)
            .delay(ONE_SECOND, TimeUnit.MILLISECONDS)
            .repeat()
            .subscribe(::showRates, ::showError)
    }

    private fun showRates(rates: PresentationRateList) = view.showCurrencyRates(rates)

    private fun showError(error: Throwable) = view.showError(error.message!!)
}
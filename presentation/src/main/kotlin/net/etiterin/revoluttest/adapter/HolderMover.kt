package net.etiterin.revoluttest.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import net.etiterin.revoluttest.model.CurrencyRateItem

private const val FIRST_POSITION = 0

class HolderMover(
    private val onBeginItemMoveListener: () -> Unit,
    private val dataset: MutableList<CurrencyRateItem>,
    private val recyclerView: RecyclerView,
    private val scrollToTop: () -> Unit,
    private val onItemClick: (CurrencyRateItem) -> Unit
) {

    private val adapter = recyclerView.adapter
    private var adapterPosition: Int = 0
    private lateinit var currentRate: CurrencyRateItem

    lateinit var currentHolder: RatesHolder

    fun moveToFirstPosition(): (View) -> Unit = {
        val currentLayoutPosition = currentHolder.layoutPosition

        if (currentLayoutPosition > FIRST_POSITION) {
            onBeginItemMoveListener()
            adapterPosition = currentHolder.adapterPosition
            currentRate = dataset[adapterPosition]

            updateWatcherPosition()
            moveCurrentRateToFirstInDataset()
            moveCurrentItemAndScrollToTop(currentLayoutPosition)
            setRatesEditTextSelectionToEnd()

            notifyItemClickedWithDelay(rate = currentRate)
        }
    }

    private fun updateWatcherPosition() {
        currentHolder.ratesWatcher.position = FIRST_POSITION
    }

    private fun moveCurrentRateToFirstInDataset() {
        dataset.removeAt(currentHolder.adapterPosition)
        dataset.add(FIRST_POSITION, currentRate)
    }

    private fun moveCurrentItemAndScrollToTop(currentLayoutPosition: Int) {
        delay(duration = 100) { adapter.notifyItemMoved(currentLayoutPosition, FIRST_POSITION) }
        delay(duration = 200) { scrollToTop() }
    }

    private fun delay(duration: Long, action: () -> Unit) {
        recyclerView.postDelayed(action, duration)
    }

    private fun setRatesEditTextSelectionToEnd() {
        delay(duration = 120) {
            val textLength = currentHolder.etCurrencyRate.text.length
            currentHolder.etCurrencyRate.requestFocus()
            currentHolder.etCurrencyRate.setSelection(textLength)
        }
    }

    private fun notifyItemClickedWithDelay(rate: CurrencyRateItem) {
        delay(duration = 300) { onItemClick(rate) }
    }
}
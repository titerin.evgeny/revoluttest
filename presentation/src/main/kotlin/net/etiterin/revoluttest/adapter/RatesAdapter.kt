package net.etiterin.revoluttest.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import net.etiterin.revoluttest.R
import net.etiterin.revoluttest.model.CurrencyRateItem

private const val FIRST_POSITION = 0

class RatesAdapter(
    val onItemClick: (CurrencyRateItem) -> Unit,
    val onBaseRateChangeListener: (CurrencyRateItem) -> Unit,
    val onBeginItemMoveListener: () -> Unit
) : RecyclerView.Adapter<RatesHolder>() {

    private val currencyRates = mutableListOf<CurrencyRateItem>()
    private lateinit var smoothScroller: SmoothScroller

    private var recyclerView: RecyclerView? = null

    init {
        setHasStableIds(true)
    }

    override fun getItemCount() = currencyRates.size

    override fun getItemId(position: Int) = currencyRates[position].currencyCode.hashCode().toLong()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatesHolder {
        val itemView = inflateItemView(parent)
        val ratesWatcher = RateChangeWatcher(onBaseRateChangeListener, currencyRates)
        val holderMover = createHolderMover()
        return RatesHolder(itemView, ratesWatcher, holderMover)
    }

    private fun inflateItemView(parent: ViewGroup): View {
        val context = parent.context
        return LayoutInflater.from(context).inflate(R.layout.rates_item, parent, false)
    }

    private fun createHolderMover() = HolderMover(
        onBeginItemMoveListener,
        currencyRates,
        recyclerView!!,
        { scrollToTop() },
        onItemClick
    )

    private fun scrollToTop() {
        smoothScroller.targetPosition = FIRST_POSITION
        recyclerView?.layoutManager?.startSmoothScroll(smoothScroller)
    }

    override fun onBindViewHolder(holder: RatesHolder, position: Int) {
        val currentRate = currencyRates[position]
        holder.bind(currentRate, position)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
        smoothScroller = SmoothScroller(recyclerView.context)
    }

    fun updateRates(currencyRates: List<CurrencyRateItem>) {
        this.currencyRates.clear()
        this.currencyRates.addAll(currencyRates)
        notifyAllItemsExceptFirst()
    }

    private fun notifyAllItemsExceptFirst() = notifyItemRangeChanged(1, currencyRates.size - 1)
}
package net.etiterin.revoluttest.adapter

import android.text.Editable
import android.text.TextWatcher
import net.etiterin.revoluttest.model.CurrencyRateItem

private const val FIRST_POSITION = 0

class RateChangeWatcher(
    private val changeListener: (CurrencyRateItem) -> Unit,
    private val dataset: List<CurrencyRateItem>
) : TextWatcher {

    var position: Int = FIRST_POSITION

    override fun afterTextChanged(s: Editable?) {}

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(changedSequence: CharSequence, start: Int, before: Int, count: Int) {
        observeChangesOnlyForFirstPosition(changedSequence)
    }

    private fun observeChangesOnlyForFirstPosition(changedSequence: CharSequence) {
        if (position == FIRST_POSITION) {
            val changedText = changedSequence.toString()
            val currentRate = dataset[position]
            currentRate.rateValue = changedText
            changeListener(currentRate)
        }
    }
}
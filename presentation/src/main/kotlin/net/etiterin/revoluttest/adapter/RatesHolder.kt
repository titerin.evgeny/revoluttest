package net.etiterin.revoluttest.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.EditText
import android.widget.TextView
import kotlinx.android.synthetic.main.rates_item.view.*
import net.etiterin.revoluttest.model.CurrencyRateItem

class RatesHolder(
    itemView: View,
    val ratesWatcher: RateChangeWatcher,
    holderMover: HolderMover
) : RecyclerView.ViewHolder(itemView) {

    val etCurrencyRate: EditText = itemView.etCurrencyRate
    private val tvCurrencyCode: TextView = itemView.tvCurrencyCode

    init {
        holderMover.currentHolder = this
        itemView.setOnClickListener(holderMover.moveToFirstPosition())
        etCurrencyRate.addTextChangedListener(ratesWatcher)
    }

    fun bind(currencyRate: CurrencyRateItem, position: Int) {
        ratesWatcher.position = position
        tvCurrencyCode.text = currencyRate.currencyCode
        etCurrencyRate.setText(currencyRate.rateValue)
    }
}
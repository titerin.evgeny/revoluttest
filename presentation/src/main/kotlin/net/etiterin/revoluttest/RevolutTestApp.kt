package net.etiterin.revoluttest

import android.app.Application
import net.etiterin.revoluttest.di.AppComponent
import net.etiterin.revoluttest.di.AppModule
import net.etiterin.revoluttest.di.DaggerAppComponent

class RevolutTestApp: Application() {

    companion object {
        lateinit var component: AppComponent
            private set
    }

    override fun onCreate() {
        super.onCreate()
        component = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }
}
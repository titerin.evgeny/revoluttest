package net.etiterin.revoluttest.rx

import io.reactivex.Scheduler

interface SchedulerProvider {

    fun androidMainScheduler(): Scheduler

    fun ioScheduler(): Scheduler
}
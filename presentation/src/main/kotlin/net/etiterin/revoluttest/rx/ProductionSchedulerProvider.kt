package net.etiterin.revoluttest.rx

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ProductionSchedulerProvider : SchedulerProvider {

    override fun androidMainScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

    override fun ioScheduler(): Scheduler {
        return Schedulers.io()
    }
}
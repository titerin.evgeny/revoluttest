package net.etiterin.revoluttest.view

import net.etiterin.revoluttest.model.CurrencyRateItem

interface MainView {

    fun showCurrencyRates(rates: List<CurrencyRateItem>)

    fun showError(errorText: String)
}
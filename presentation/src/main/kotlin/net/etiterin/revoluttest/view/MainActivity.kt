package net.etiterin.revoluttest.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import net.etiterin.revoluttest.R
import net.etiterin.revoluttest.adapter.RatesAdapter
import net.etiterin.revoluttest.RevolutTestApp
import net.etiterin.revoluttest.di.DaggerMainViewComponent
import net.etiterin.revoluttest.di.MainScreenModule
import net.etiterin.revoluttest.model.CurrencyRateItem
import net.etiterin.revoluttest.presenter.MainPresenter
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainView {

    private lateinit var ratesAdapter: RatesAdapter

    @Inject
    lateinit var mainPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependencies()
        setContentView(R.layout.activity_main)

        initRecyclerView()
        mainPresenter.start(this@MainActivity)
        mainPresenter.loadRates()
    }

    private fun injectDependencies() {
        DaggerMainViewComponent.builder()
            .appComponent(RevolutTestApp.component)
            .mainScreenModule(MainScreenModule())
            .build()
            .inject(this)
    }

    private fun initRecyclerView() {
        ratesAdapter = RatesAdapter(
            onItemClick = mainPresenter::loadRates,
            onBaseRateChangeListener = mainPresenter::loadRates,
            onBeginItemMoveListener = mainPresenter::unSubscribeAll
        )

        ratesRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = ratesAdapter
        }
    }

    override fun showCurrencyRates(rates: List<CurrencyRateItem>) {
        ratesAdapter.updateRates(rates)
    }

    override fun showError(errorText: String) {
        Toast.makeText(this, errorText, Toast.LENGTH_LONG).show()
    }
}

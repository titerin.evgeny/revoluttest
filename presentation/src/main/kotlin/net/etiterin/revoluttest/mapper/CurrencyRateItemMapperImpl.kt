package net.etiterin.revoluttest.mapper

import net.etiterin.domain.model.CurrencyRate
import net.etiterin.revoluttest.model.CurrencyRateItem
import java.math.BigDecimal
import java.text.DecimalFormat

private const val FORMATTING_MASK = "0.00"

class CurrencyRateItemMapperImpl : CurrencyRateItemMapper {

    private val formatter = DecimalFormat(FORMATTING_MASK)

    override fun mapFromDomainList(list: DomainRateList): PresentationRateList {
        return list.map { rate ->
            mapFromDomainRate(rate)
        }.toMutableList()
    }

    private fun mapFromDomainRate(rate: CurrencyRate): CurrencyRateItem {
        val currencyCode = rate.currencyCode
        val rateValue = formatBigDecimalToString(rate.rateValue)

        return CurrencyRateItem(currencyCode, rateValue)
    }

    private fun formatBigDecimalToString(value: BigDecimal): String {
        return formatter.format(value)
    }

    override fun mapToDomainRate(rate: CurrencyRateItem): CurrencyRate {
        val currencyCode = rate.currencyCode
        val rateValue = getBigDecimalOrZero(rate.rateValue)

        return CurrencyRate(currencyCode, rateValue)
    }

    private fun getBigDecimalOrZero(string: String): BigDecimal {
        return if (string.isBlank()) {
            BigDecimal.ZERO
        } else {
            string.toBigDecimal()
        }
    }
}
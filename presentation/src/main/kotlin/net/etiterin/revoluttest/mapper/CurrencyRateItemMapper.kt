package net.etiterin.revoluttest.mapper

import net.etiterin.domain.model.CurrencyRate
import net.etiterin.revoluttest.model.CurrencyRateItem

typealias DomainRateList = MutableList<CurrencyRate>
typealias PresentationRateList = MutableList<CurrencyRateItem>

interface CurrencyRateItemMapper {

    fun mapFromDomainList(list: DomainRateList): PresentationRateList

    fun mapToDomainRate(rate: CurrencyRateItem): CurrencyRate
}
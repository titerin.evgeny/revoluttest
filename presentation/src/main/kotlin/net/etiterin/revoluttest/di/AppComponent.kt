package net.etiterin.revoluttest.di

import dagger.Component
import net.etiterin.domain.repository.CurrencyRatesRepository
import net.etiterin.revoluttest.rx.SchedulerProvider
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun provideRepo(): CurrencyRatesRepository

    fun provideSchedulers(): SchedulerProvider
}
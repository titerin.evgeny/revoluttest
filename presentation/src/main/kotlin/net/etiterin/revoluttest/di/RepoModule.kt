package net.etiterin.revoluttest.di

import dagger.Module
import dagger.Provides
import net.etiterin.data.di.LocalSource
import net.etiterin.data.di.RemoteSource
import net.etiterin.data.mapper.CurrencyRatesMapperImpl
import net.etiterin.data.repository.CurrencyRatesRepositoryImpl
import net.etiterin.data.repository.LocalRatesSource
import net.etiterin.data.repository.RatesDataSource
import net.etiterin.data.repository.RemoteRatesSource
import net.etiterin.data.service.RevolutService
import net.etiterin.domain.repository.CurrencyRatesRepository
import javax.inject.Singleton

@Module
class RepoModule {

    @Singleton
    @Provides
    @LocalSource
    fun provideLocalRatesSource(): RatesDataSource {
        return LocalRatesSource()
    }

    @Singleton
    @Provides
    @RemoteSource
    fun provideRemoteRatesSource(service: RevolutService): RatesDataSource {
        return RemoteRatesSource(service, CurrencyRatesMapperImpl())
    }

    @Singleton
    @Provides
    fun provideRatesRepository(
        @RemoteSource remote: RatesDataSource,
        @LocalSource local: RatesDataSource
    ): CurrencyRatesRepository {
        return CurrencyRatesRepositoryImpl(remote, local)
    }
}
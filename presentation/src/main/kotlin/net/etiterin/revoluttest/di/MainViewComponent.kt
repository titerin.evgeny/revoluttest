package net.etiterin.revoluttest.di

import dagger.Component
import net.etiterin.revoluttest.view.MainActivity

@PerScreen
@Component(
    modules = [MainScreenModule::class],
    dependencies = [AppComponent::class]
)
interface MainViewComponent {

    fun inject(view: MainActivity)
}
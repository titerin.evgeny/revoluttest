package net.etiterin.revoluttest.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import net.etiterin.data.repository.createRevolutService
import net.etiterin.data.service.RevolutService
import net.etiterin.revoluttest.rx.ProductionSchedulerProvider
import net.etiterin.revoluttest.rx.SchedulerProvider
import javax.inject.Singleton

@Module(includes = [RepoModule::class])
class AppModule(private val application: Application) {

    @Singleton
    @Provides
    fun provideContext(): Context = application.applicationContext

    @Singleton
    @Provides
    fun provideSchedulers(): SchedulerProvider {
        return ProductionSchedulerProvider()
    }

    @Singleton
    @Provides
    fun provideRevolutService(): RevolutService {
        return createRevolutService()
    }
}
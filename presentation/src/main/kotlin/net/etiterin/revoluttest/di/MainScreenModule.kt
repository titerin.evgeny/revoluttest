package net.etiterin.revoluttest.di

import dagger.Module
import dagger.Provides
import net.etiterin.domain.repository.CurrencyRatesRepository
import net.etiterin.domain.usecase.CurrencyRatesUseCase
import net.etiterin.revoluttest.mapper.CurrencyRateItemMapper
import net.etiterin.revoluttest.mapper.CurrencyRateItemMapperImpl
import net.etiterin.revoluttest.presenter.MainPresenter
import net.etiterin.revoluttest.rx.SchedulerProvider

@Module
class MainScreenModule {

    @PerScreen
    @Provides
    fun provideCurrencyRatesUseCase(repository: CurrencyRatesRepository): CurrencyRatesUseCase {
        return CurrencyRatesUseCase(repository)
    }

    @PerScreen
    @Provides
    fun provideMainPresenter(
        ratesUseCase: CurrencyRatesUseCase,
        schedulerProvider: SchedulerProvider,
        mapper: CurrencyRateItemMapper
    ): MainPresenter {
        return MainPresenter(ratesUseCase, schedulerProvider, mapper)
    }

    @PerScreen
    @Provides
    fun provideCurrencyRateItemMapper(): CurrencyRateItemMapper {
        return CurrencyRateItemMapperImpl()
    }
}
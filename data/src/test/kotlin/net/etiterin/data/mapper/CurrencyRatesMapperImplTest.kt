package net.etiterin.data.mapper

import junit.framework.Assert.assertEquals
import net.etiterin.data.model.CurrencyRatesData.Companion.exampleCurrencyEntity
import net.etiterin.domain.model.CurrencyRatesData.Companion.exampleCurrencyRatesList
import org.junit.Test

class CurrencyRatesMapperImplTest {

    private val mapper = CurrencyRatesMapperImpl()

    @Test
    fun `should map currency rates from entity to list`() {
        val entity = exampleCurrencyEntity
        val expectedList = exampleCurrencyRatesList

        val actualList = mapper.mapFromEntityToList(entity)

        assertEquals(expectedList, actualList)
    }
}
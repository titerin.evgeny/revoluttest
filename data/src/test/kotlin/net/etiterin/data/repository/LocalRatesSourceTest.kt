package net.etiterin.data.repository

import net.etiterin.domain.model.CurrencyRate
import net.etiterin.domain.model.CurrencyRatesData.Companion.exampleCurrencyRatesList
import org.junit.Test

class LocalRatesSourceTest {

    private val localSource = LocalRatesSource()

    @Test
    fun `should get empty list if no value found`() {
        val expectedList = mutableListOf<CurrencyRate>()

        val testObserver = localSource.getCurrencyRates(baseCurrencyCode = "RUB").test()

        testObserver.assertResult(expectedList)
        testObserver.assertComplete()
    }

    @Test
    fun `should get previously saved list`() {
        val expectedList = exampleCurrencyRatesList
        localSource.saveRates(baseCurrencyCode = "RUB", rates = expectedList).subscribe()

        val testObserver = localSource.getCurrencyRates(baseCurrencyCode = "RUB").test()

        testObserver.assertResult(expectedList)
        testObserver.assertComplete()
    }
}
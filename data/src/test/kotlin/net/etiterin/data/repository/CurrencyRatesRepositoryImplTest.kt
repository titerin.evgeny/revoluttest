package net.etiterin.data.repository

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import net.etiterin.domain.model.CurrencyRate
import net.etiterin.domain.model.CurrencyRatesData.Companion.exampleCurrencyRatesList
import org.junit.Test

class CurrencyRatesRepositoryImplTest {

    private val remoteSource: RatesDataSource = mock()
    private val localSource: RatesDataSource = mock()

    private val repository = CurrencyRatesRepositoryImpl(remoteSource, localSource)


    @Test
    fun `should get remote currency rates`() {
        val expectedList = exampleCurrencyRatesList
        val listSingle = Single.just(expectedList)
        whenever(remoteSource.getCurrencyRates("RUB")).thenReturn(listSingle)

        val testObserver = repository.getCurrencyRates(baseCurrencyCode = "RUB", isCached = false).test()

        verify(localSource).saveRates(baseCurrencyCode = "RUB", rates = expectedList)
        testObserver.assertResult(expectedList)
        testObserver.assertComplete()
    }

    @Test
    fun `should get cached currency rates`() {
        val expectedList = exampleCurrencyRatesList
        val listSingle = Single.just(expectedList)
        val emptyListSingle = Single.just(mutableListOf<CurrencyRate>())
        whenever(remoteSource.getCurrencyRates("RUB")).thenReturn(emptyListSingle)
        whenever(localSource.getCurrencyRates("RUB")).thenReturn(listSingle)

        val testObserver = repository.getCurrencyRates(baseCurrencyCode = "RUB", isCached = true).test()

        verify(localSource).saveRates(baseCurrencyCode = "RUB", rates = expectedList)
        testObserver.assertResult(expectedList)
        testObserver.assertComplete()
    }
}
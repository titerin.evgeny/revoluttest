package net.etiterin.data.repository

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import net.etiterin.data.mapper.CurrencyRatesMapper
import net.etiterin.data.model.CurrencyRatesData
import net.etiterin.data.service.RevolutService
import net.etiterin.domain.model.CurrencyRatesData.Companion.exampleCurrencyRatesList
import org.junit.Test

class RemoteRatesSourceTest {

    private val service: RevolutService = mock()
    private val mapper: CurrencyRatesMapper = mock()

    private val remoteSource = RemoteRatesSource(service, mapper)

    @Test
    fun `should get rates from remote source`() {
        val entity = CurrencyRatesData.exampleCurrencyEntity
        val expectedList = exampleCurrencyRatesList
        whenever(service.getCurrencyRates("RUB")).thenReturn(Single.just(entity))
        whenever(mapper.mapFromEntityToList(entity)).thenReturn(expectedList)

        val testObserver = remoteSource.getCurrencyRates("RUB").test()

        testObserver.assertResult(expectedList)
        testObserver.assertComplete()
    }

    @Test
    fun `should throw exception if try to save to remote`() {
        val expectedErrorClass = UnsupportedOperationException::class.java
        val expectedErrorMessage = "Can't save rates to remote data source!"

        val testObserver = remoteSource.saveRates(baseCurrencyCode = "RUB", rates = mutableListOf()).test()

        testObserver.assertError(expectedErrorClass)
        testObserver.assertErrorMessage(expectedErrorMessage)
    }
}
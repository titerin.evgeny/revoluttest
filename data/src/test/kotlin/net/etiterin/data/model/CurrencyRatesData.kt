package net.etiterin.data.model

import java.math.BigDecimal

class CurrencyRatesData {
    companion object {

        val exampleCurrencyEntity = CurrencyRatesEntity(
            baseCurrency = "RUB",
            ratesList = RevolutCurrencies(
                AUD = BigDecimal("0.020352"),
                BGN = BigDecimal("0.024625"),
                BRL = BigDecimal("0.060333"),
                CAD = BigDecimal("0.019311"),
                CHF = BigDecimal("0.014197"),
                CNY = BigDecimal("0.10004"),
                CZK = BigDecimal("0.32377"),
                DKK = BigDecimal("0.093886"),
                EUR = BigDecimal("0.012591"),
                GBP = BigDecimal("0.01131"),
                HKD = BigDecimal("0.11498"),
                HRK = BigDecimal("0.093601"),
                HUF = BigDecimal("4.1107"),
                IDR = BigDecimal("218.12"),
                ILS = BigDecimal("0.052512"),
                INR = BigDecimal("1.0541"),
                ISK = BigDecimal("1.609"),
                JPY = BigDecimal("1.6312"),
                KRW = BigDecimal("16.429"),
                MXN = BigDecimal("0.2816"),
                MYR = BigDecimal("0.060587"),
                NOK = BigDecimal("0.12309"),
                NZD = BigDecimal("0.022202"),
                PHP = BigDecimal("0.78809"),
                PLN = BigDecimal("0.054371"),
                RON = BigDecimal("0.058403"),
                RUB = null,
                SEK = BigDecimal("0.13335"),
                SGD = BigDecimal("0.020146"),
                THB = BigDecimal("0.48009"),
                TRY = BigDecimal("0.096045"),
                USD = BigDecimal("0.014648"),
                ZAR = BigDecimal("0.2244")
            )
        )

        const val exampleResponse =
            """
            {
                "base": "RUB",
                "date": "2018-09-06",
                "rates": {
                    "AUD": 0.020352,
                    "BGN": 0.024625,
                    "BRL": 0.060333,
                    "CAD": 0.019311,
                    "CHF": 0.014197,
                    "CNY": 0.10004,
                    "CZK": 0.32377,
                    "DKK": 0.093886,
                    "EUR": 0.012591,
                    "GBP": 0.01131,
                    "HKD": 0.11498,
                    "HRK": 0.093601,
                    "HUF": 4.1107,
                    "IDR": 218.12,
                    "ILS": 0.052512,
                    "INR": 1.0541,
                    "ISK": 1.609,
                    "JPY": 1.6312,
                    "KRW": 16.429,
                    "MXN": 0.2816,
                    "MYR": 0.060587,
                    "NOK": 0.12309,
                    "NZD": 0.022202,
                    "PHP": 0.78809,
                    "PLN": 0.054371,
                    "RON": 0.058403,
                    "SEK": 0.13335,
                    "SGD": 0.020146,
                    "THB": 0.48009,
                    "TRY": 0.096045,
                    "USD": 0.014648,
                    "ZAR": 0.2244
                }
            }
            """
    }
}
package net.etiterin.data.model

import com.google.gson.Gson
import net.etiterin.data.model.CurrencyRatesData.Companion.exampleCurrencyEntity
import net.etiterin.data.model.CurrencyRatesData.Companion.exampleResponse
import org.junit.Assert.assertEquals
import org.junit.Test

class CurrencyRatesEntityTest {

    private val gson = Gson()

    @Test
    fun `should de-serialize revolut currency rates response`() {
        val expectedResponse = exampleCurrencyEntity

        val actualResponse = gson.fromJson(exampleResponse, CurrencyRatesEntity::class.java)

        assertEquals(expectedResponse, actualResponse)
    }

}
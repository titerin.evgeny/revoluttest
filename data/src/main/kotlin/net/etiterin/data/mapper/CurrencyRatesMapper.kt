package net.etiterin.data.mapper

import net.etiterin.data.model.CurrencyRatesEntity
import net.etiterin.domain.model.CurrencyRate

interface CurrencyRatesMapper {

    fun mapFromEntityToList(entity: CurrencyRatesEntity): MutableList<CurrencyRate>
}
package net.etiterin.data.mapper

import net.etiterin.data.model.CurrencyRatesEntity
import net.etiterin.data.model.RevolutCurrencies
import net.etiterin.domain.model.CurrencyRate
import java.math.BigDecimal
import kotlin.reflect.full.memberProperties

class CurrencyRatesMapperImpl : CurrencyRatesMapper {

    override fun mapFromEntityToList(entity: CurrencyRatesEntity): MutableList<CurrencyRate> {
        val rates = entity.ratesList
        return mapRatesToList(rates)
    }

    private fun mapRatesToList(rates: RevolutCurrencies): MutableList<CurrencyRate> {
        val list: MutableList<CurrencyRate> = ArrayList()

        RevolutCurrencies::class.memberProperties.forEach { member ->
            val currencyCode = member.name
            val rateValue = member.getter.call(rates) as BigDecimal?

            rateValue?.let {
                val currencyRate = CurrencyRate(currencyCode, rateValue)
                list.add(currencyRate)
            }
        }

        return list
    }
}
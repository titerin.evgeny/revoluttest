package net.etiterin.data.repository

import io.reactivex.Single
import net.etiterin.domain.model.CurrencyRate
import net.etiterin.domain.repository.CurrencyRatesRepository

private typealias RatesList = MutableList<CurrencyRate>

class CurrencyRatesRepositoryImpl(
    private val remoteRatesSource: RatesDataSource,
    private val localRatesSource: RatesDataSource
) : CurrencyRatesRepository {

    override fun getCurrencyRates(baseCurrencyCode: String, isCached: Boolean): Single<RatesList> {

        return if (isCached) {
            getCachedRatesOrRemote(baseCurrencyCode)
        } else {
            remoteRatesSource.getCurrencyRates(baseCurrencyCode)
                .map { rates ->
                    putToCache(baseCurrencyCode, rates)
                    rates
                }
        }
    }

    private fun getCachedRatesOrRemote(baseCurrencyCode: String): Single<RatesList> {

        return Single.concat(
            localRatesSource.getCurrencyRates(baseCurrencyCode),
            remoteRatesSource.getCurrencyRates(baseCurrencyCode)
        )
            .filter { rates -> !rates.isEmpty() }
            .firstOrError()
            .map { rates ->
                putToCache(baseCurrencyCode, rates)
                rates
            }
    }

    private fun putToCache(baseCurrencyCode: String, rates: RatesList) {
        localRatesSource.saveRates(baseCurrencyCode, rates)
    }
}
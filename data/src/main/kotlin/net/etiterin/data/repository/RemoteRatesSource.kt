package net.etiterin.data.repository

import io.reactivex.Completable
import io.reactivex.Single
import net.etiterin.data.mapper.CurrencyRatesMapper
import net.etiterin.data.service.RevolutService
import net.etiterin.domain.model.CurrencyRate

class RemoteRatesSource(
    private val revolutService: RevolutService,
    private val mapper: CurrencyRatesMapper
) : RatesDataSource {

    override fun getCurrencyRates(baseCurrencyCode: String): Single<MutableList<CurrencyRate>> {
        return revolutService.getCurrencyRates(baseCurrencyCode)
            .map { entity ->
                mapper.mapFromEntityToList(entity)
            }
    }

    override fun saveRates(
        baseCurrencyCode: String,
        rates: MutableList<CurrencyRate>
    ): Completable {
        val error = UnsupportedOperationException("Can't save rates to remote data source!")
        return Completable.error(error)
    }
}
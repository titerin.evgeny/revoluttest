package net.etiterin.data.repository

import io.reactivex.Completable
import io.reactivex.Single
import net.etiterin.domain.model.CurrencyRate
import java.util.concurrent.ConcurrentHashMap

class LocalRatesSource : RatesDataSource {

    private val ratesMap: MutableMap<String, MutableList<CurrencyRate>> = ConcurrentHashMap()

    override fun getCurrencyRates(baseCurrencyCode: String): Single<MutableList<CurrencyRate>> {
        return Single.just(
            getRatesOrEmptyList(baseCurrencyCode)
        )
    }

    private fun getRatesOrEmptyList(baseCurrencyCode: String): MutableList<CurrencyRate> {
        return ratesMap[baseCurrencyCode] ?: mutableListOf()
    }

    override fun saveRates(
        baseCurrencyCode: String,
        rates: MutableList<CurrencyRate>
    ): Completable {
        return Completable.fromAction { ratesMap[baseCurrencyCode] = rates }
    }
}
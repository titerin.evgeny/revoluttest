package net.etiterin.data.repository

import net.etiterin.data.service.RevolutService
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

private const val REVOLUT_BASE_URL = "https://revolut.duckdns.org/"

fun createRevolutService(): RevolutService {
    return createRetrofitInstance().create(RevolutService::class.java)
}

private fun createRetrofitInstance(): Retrofit {
    return Retrofit.Builder()
        .baseUrl(REVOLUT_BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}
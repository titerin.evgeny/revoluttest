package net.etiterin.data.repository

import io.reactivex.Completable
import io.reactivex.Single
import net.etiterin.domain.model.CurrencyRate

interface RatesDataSource {

    fun getCurrencyRates(baseCurrencyCode: String): Single<MutableList<CurrencyRate>>

    fun saveRates(
        baseCurrencyCode: String,
        rates: MutableList<CurrencyRate>
    ): Completable
}
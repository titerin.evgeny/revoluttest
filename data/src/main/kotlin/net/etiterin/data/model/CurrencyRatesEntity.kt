package net.etiterin.data.model

import com.google.gson.annotations.SerializedName

data class CurrencyRatesEntity(

    @SerializedName("base")
    val baseCurrency: String,

    @SerializedName("rates")
    val ratesList: RevolutCurrencies
)
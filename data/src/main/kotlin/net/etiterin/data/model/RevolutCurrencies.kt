package net.etiterin.data.model

import java.math.BigDecimal

data class RevolutCurrencies(

    val AUD: BigDecimal?,
    val BGN: BigDecimal?,
    val BRL: BigDecimal?,
    val CAD: BigDecimal?,
    val CHF: BigDecimal?,
    val CNY: BigDecimal?,
    val CZK: BigDecimal?,
    val DKK: BigDecimal?,
    val EUR: BigDecimal?,
    val GBP: BigDecimal?,
    val HKD: BigDecimal?,
    val HRK: BigDecimal?,
    val HUF: BigDecimal?,
    val IDR: BigDecimal?,
    val ILS: BigDecimal?,
    val INR: BigDecimal?,
    val ISK: BigDecimal?,
    val JPY: BigDecimal?,
    val KRW: BigDecimal?,
    val MXN: BigDecimal?,
    val MYR: BigDecimal?,
    val NOK: BigDecimal?,
    val NZD: BigDecimal?,
    val PHP: BigDecimal?,
    val PLN: BigDecimal?,
    val RON: BigDecimal?,
    val RUB: BigDecimal?,
    val SEK: BigDecimal?,
    val SGD: BigDecimal?,
    val THB: BigDecimal?,
    val TRY: BigDecimal?,
    val USD: BigDecimal?,
    val ZAR: BigDecimal?
)
package net.etiterin.data.service

import io.reactivex.Single
import net.etiterin.data.model.CurrencyRatesEntity
import retrofit2.http.GET
import retrofit2.http.Query

interface RevolutService {

    @GET("latest")
    fun getCurrencyRates(@Query("base") baseCurrencyCode: String): Single<CurrencyRatesEntity>
}